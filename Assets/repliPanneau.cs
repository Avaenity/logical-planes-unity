﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class repliPanneau : MonoBehaviour
{
    public Transform stick;
    public Transform panel;
    public Transform testBaton;


    // Start is called before the first frame update
    void Start()
    {
        Sequence sequence = DOTween.Sequence();

        sequence
            .AppendInterval(3f)
            .Append(transform.DOMoveY(2.25f, 0.4f).SetEase(Ease.InBack))
            .Append(stick.DORotate(new Vector3(0f, 0f, 720f), 0.6f, RotateMode.FastBeyond360).SetRelative(true))
            .Append(transform.DOMoveY(-0.4f, 0.5f).SetEase(Ease.OutBounce))
        //.Append(stickParent.DOScale(new Vector3(1f, 4f, 1f), 0.5f).SetEase(Ease.OutBounce))
        //.AppendInterval(0.4f)
        .Append(panel.DOScale(new Vector3(1.9F, 1.2f, 0.08f), 0.5f).SetEase(Ease.OutBounce))
        .AppendInterval(10f)
        .Append(panel.DOScale(new Vector3(0f, 0f, 0f), 0.5f).SetEase(Ease.OutBounce))
        //    .AppendInterval(0.4f)
        //    .Append(stickParent.DOScale(new Vector3(1f, 1f, 1f), 0.5f).SetEase(Ease.OutBounce))
        .Append(stick.DOMoveY(-0.59f, 0.4f));
        // .Join(panel.DOScale(new Vector3(0.7F, 0.1f, 0.8f), 0.8f).SetEase(Ease.OutBounce))
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UnfoldPanel()
    {
        Sequence sequence = DOTween.Sequence();

        sequence
            .Append(panel.DOScale(new Vector3(0f, 0f, 0f), 0.5f).SetEase(Ease.OutBounce))
            .AppendInterval(3f)
         //   .Append(stickParent.DOScale(new Vector3(1f, 1f, 1f), 0.5f).SetEase(Ease.OutBounce))
            .Append(transform.DOMoveY(0.59f, 0.4f));
        // .Join(panel.DOScale(new Vector3(0.7F, 0.1f, 0.8f), 0.8f).SetEase(Ease.OutBounce))

    }

}
