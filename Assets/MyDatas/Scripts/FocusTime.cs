﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

public class FocusTime : MonoBehaviour
{

    private Tween scaleTween;

    public void Focus(BaseEventData bed = null)
    {
        Debug.Log("currentInputModule" + bed.currentInputModule.name);
        bed.Use();
        Debug.Log(bed.used);
        if(bed.selectedObject != null)
            Debug.Log(bed.selectedObject.name);

        scaleTween = transform.DOScale(0.3f, 1.5f).OnComplete(AddScore);

    }

    public void Kill()
    {
        scaleTween.Kill();
    }

    public void Unfocus()
    {
        Debug.Log("UnFocusing");

        Kill();
        scaleTween = transform.DOScale(0.005f, 0.03f);
    }

    private void AddScore() {
        Unfocus();
        Debug.Log("Add score");
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0));
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
        {
            Destroy(hit.collider.gameObject);
        }

    }

}
